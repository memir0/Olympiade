#include<bits/stdc++.h>
using namespace std;
# define INF 0x3f3f3f3f

class Graph
{
    int V;    
    list< pair<int, int> > *adj;
 
public:
    Graph(int V);  
 
    void addEdge(int u, int v, int w);
 
    int shortestPath(int s, int x);
};

Graph::Graph(int V)
{
    this->V = V;
    adj = new list< pair<int, int> >[V];
}
 
void Graph::addEdge(int u, int v, int w)
{
    adj[u].push_back(make_pair(v, w));
    adj[v].push_back(make_pair(u, w));
}
 
int Graph::shortestPath(int src, int x)
{
    set< pair<int, int> > setds;
 
    vector<int> dist(V, INF);

    setds.insert(make_pair(0, src));
    dist[src] = 0;
 
    while (!setds.empty())
    {
        pair<int, int> tmp = *(setds.begin());
        setds.erase(setds.begin());
 
        int u = tmp.second;
 
        list< pair<int, int> >::iterator i;
        for (i = adj[u].begin(); i != adj[u].end(); ++i)
        {
            int v = (*i).first;
            int weight = (*i).second;
            if (dist[v] > dist[u] + weight)
            {
                if (dist[v] != INF)
                    setds.erase(setds.find(make_pair(dist[v], v)));

                dist[v] = dist[u] + weight;
                if (dist[v] >= x)
                    return INF; //Hvis den er lengre enn en vi allerede har sett på stopper vi
                setds.insert(make_pair(dist[v], v));
            }
        }
    }
 
    int largest = 0;
    for (int i = 0; i < V; ++i){
        if (largest < dist[i])
            largest = dist[i];
    }
    return largest;
}
 
int main()
{
    int N, K;
    cin >> N >> K;
    
    int V = N;
    Graph g(V);
    
    for(int i = 0; i < K; i++) {
        int a, b, c;
        cin >> a >> b >> c;
        g.addEdge(a, b, c);
    }
    int hovedstad = -1;
    int x = 2147483647;
    for(int i=0; i<N; i++){
        int shorty = g.shortestPath(i, x);
        if  (x > shorty){
            hovedstad = i;
            x = shorty;
        }
    }
    cout << hovedstad;
    return 0;
}