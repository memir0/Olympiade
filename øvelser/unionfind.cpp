#include "stdafx.h"
#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

class unionFind {
private:
	vector<int> p, rank, setSize;
	int numSets; //antall grupper
				 //p er lederen til hver node
				 //rank er for optimalisering når vi slår sammen
				 //setSize er antall noder i gruppen
public:
	unionFind(int N) { //funksjon og ikke type som class
		setSize.assign(N, 1); //er kun 1 i hver gruppe
		numSets = N; //N antall grupper
		rank.assign(N, 0);
		p.assign(N, 0);
		for (int i = 0; i < N; i++) {
			p[i] = i;
		}
	}
	int findSet(int i) {
		return (p[i] == i) ? i : (p[i] = findSet(p[i]));
		//Hvis noden er dens egen leder returner noden, else gjør den den samme for lederen dens til den finner lederen
		//(p[i] = findSet(p[i]) er kun for å optimalisere slik at den husker hvem som er lederen til neste gang
	}
	bool isSameSet(int i, int j) { return findSet(i) == findSet(j); }

	void unionSet(int i, int j) {
		if (!isSameSet(i, j)) {
			numSets--;
			int x = findSet(i), y = findSet(j);

			// rank holder avstanden fra lederen til sine medlemer kort
			if (rank[x] > rank[y]) {
				p[y] = x;
				setSize[x] += setSize[y];
			}
			else {
				p[x] = x;
				setSize[x] += setSize[y];
				if (rank[x] == rank[y]) {
					rank[y]++;
				}
			}
		}
	}
	int numDisjointSets() { return numSets; }
	int sizeOfSet(int i) { return setSize[findSet(i)]; }
};

int main() {
	unionFind uf(4);
	uf.unionSet(0, 1); //Gates og Brin blir venner
	uf.unionSet(2, 3); //Zuckerberg og Jobs blir venner
	if (uf.isSameSet(0, 3)) {
		cout << "Gates og Zuckerberg er venner \n";
	} else {
		cout << "Gates og Zuckerberg er ikke venner \n";
	}
	cout << "Antall grupper: " << uf.numDisjointSets() << endl;
	cout << "Antall personer i gruppen som Brin er med i: " << uf.sizeOfSet(1) << endl;
	
	uf.unionSet(1, 3);
	cout << "Lederen til Zuckerberg er: " << uf.findSet(3) << endl;
}