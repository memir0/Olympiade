#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

int main(){
    int N, T, tmp = 0;
    vector<int> a, p;
    cout << "Antall hus og antall tomter\n";
    cin >> N >> T;
    cout << "hus\n";
    for(int i = 0; i < N; i++){
        cin >> tmp;
        a.push_back(tmp);
    }
    cout << "tomter\n";
    for(int i = 0; i < T; i++){
        cin >> tmp;
        p.push_back(tmp);
    }
    
    sort(begin(a), end(a));
    sort(begin(p), end(p));
    
    for (int i=0; i < T; i++){
        int low = 0;
        int high = a.size() - 1;
        int tomt = p[i];
        
        while(high != low+1){
            if(tomt < a[(high-low)/2+low]){
                high = (high-low)/2+low;
            }
            else{
                low = (high-low)/2+low;
            }
        }
        if(tomt-a[low] < a[high]-tomt){
            cout << "low: " << tomt-a[low] << "\n";
        }
        else{
            cout << "high: " << abs(tomt - a[high]) << "\n";
        }
    }
}