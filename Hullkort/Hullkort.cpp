#include <iostream>
#include <math.h>
using namespace std;

int main(){
	unsigned long long N, B;
	unsigned long long tmp1 = 2;
	unsigned long long tmp2 = 2;
	unsigned long long tmp3 = 2;
	cin >> N;
	if((N % 2) == 0){
		for(int i = 0; i<(N%1000000007)-1; i++){
		    tmp2 = (tmp2 * 2)%1000000007;
		    if(i<(N/2)-1){
		        tmp1 = (tmp1 * 2)%1000000007;
		    }
		}
		tmp3 = ((tmp1 + tmp2 - 2)*500000004)%1000000007;
	}
	else{
		for(int i = 0; i<(N%1000000007)-1; i++){
		    tmp2 = (tmp2 * 2)%1000000007;
		    if(i<(N/2)){
		        tmp1 = (tmp1 * 2)%1000000007;
		    }
		}
		tmp3 = ((tmp1 + tmp2 - 2)*500000004)%1000000007;
	}
	cout << tmp3;
}