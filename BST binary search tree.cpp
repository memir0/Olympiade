// BST binary search tree.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <vector>
using namespace std;

struct sNode {
	int value;
	int left, right;
	int parent;

	sNode(int _value, int _left, int _right, int _parent) {
		value = _value;
		left = _left;
		right = _right;
		parent = _parent;
	}
};

vector<sNode> node; //node[0] er rot-noden

//returnerer indeksen til noden med verdi = int value
int finnNode(int value) {
	//returnerer -1 dersom noden ikke finnes
	if (node.size() == 0)
		return -1;

	int curNode = 0;
	while (curNode != -1 && node[curNode].value != value) {
		if (value < node[curNode].left) {
			curNode = node[curNode].left;
		} else {
			curNode = node[curNode].right;
		}
	}
	return curNode;
}

int finnParentNode(int value) {
	//returnerer -1 hvis det ikke finnes noen rot-node
	if (node.size() == 0)
		return -1;

	int curNode = 0;
	while (true) {
		int nxtNode;
		if (value < node[curNode].value) {
			nxtNode = node[curNode].left;
		} else {
			nxtNode = node[curNode].right;
		}

		if (nxtNode == -1) {
			return curNode;
		}
		curNode = nxtNode;
	}
}

//legger til en node i treet med verdi lik value
void leggTilNode(int value) {
	int parent = finnParentNode(value);

	//Sjekker om det finnes en parent-node
	if (parent != -1) {
		if (value < node[parent].value) {
			node[parent].left = node.size();
		} else {
			node[parent].right = node.size();
		}
	}
	node.push_back(sNode(value, -1, -1, parent));
}

//Skriver ut alle nodene i venstre subtre før den skriver ut seg selv (idx)
//for så å skrive ut høyre subtre
void leftPrint(int idx) {
	if (idx == -1)
		return;
	leftPrint(node[idx].left);
	cout << node[idx].value << endl;
	leftPrint(node[idx].right);
}

//Samme, men begyner i høyre
void rightPrint(int idx) {
	if (idx == -1)
		return;
	leftPrint(node[idx].right);
	cout << node[idx].value << endl;
	leftPrint(node[idx].left);
}

int main()
{
	int N;
	cin >> N;
	
	int tmp;
	for (int i = 0; i < N; i++) {
		cin >> tmp;
		leggTilNode(tmp);
	}
	cout << "\n Left Print: \n";
	leftPrint(0);
	cout << "________________ \n";
	cout << "Right Print: \n";
	rightPrint(0);
    return 0;
}